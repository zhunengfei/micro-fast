package com.micro.fast.monitor.center;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import zipkin.server.EnableZipkinServer;

/**
 * @author lsy
 */
@SpringCloudApplication
//集群熔断降级情况的聚合展示
@EnableTurbine
//集群间服务调用情况的聚合展示
@EnableZipkinServer
@EnableHystrixDashboard
@EnableHystrix
public class MsMonitorCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsMonitorCenterApplication.class, args);
	}
}
