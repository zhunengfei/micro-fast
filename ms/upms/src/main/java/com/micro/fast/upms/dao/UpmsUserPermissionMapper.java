package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsUserPermission;

public interface UpmsUserPermissionMapper {
    int deleteByPrimaryKey(Integer userPermissionId);

    int insert(UpmsUserPermission record);

    int insertSelective(UpmsUserPermission record);

    UpmsUserPermission selectByPrimaryKey(Integer userPermissionId);

    int updateByPrimaryKeySelective(UpmsUserPermission record);

    int updateByPrimaryKey(UpmsUserPermission record);
}