package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsOrganization;

public interface UpmsOrganizationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsOrganization record);

    int insertSelective(UpmsOrganization record);

    UpmsOrganization selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsOrganization record);

    int updateByPrimaryKey(UpmsOrganization record);
}