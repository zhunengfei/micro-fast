package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsUser;
import feign.Param;

/**
 * @author lsy
 */
public interface UpmsUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsUser record);

    int insertSelective(UpmsUser record);

    UpmsUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsUser record);

    int updateByPrimaryKey(UpmsUser record);

    /**
     * 根据用户名查找用户
     * @param username 用户名
     * @return
     */
    UpmsUser selectByUsername(@Param("username") String username);

    /**
     * 检查用户名是否存在
     * @param username
     * @return
     */
    int countUsername(String username);

    /**
     * 检查邮箱是否被占用
     * @param email
     * @return
     */
    int countEmail(String email);
}